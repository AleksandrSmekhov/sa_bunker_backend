from django.urls import path
from channels.routing import URLRouter

from game.routing import ws_urlpatterns as game_ws_urlpatterns

from config.settings import ENV


ws_urlpatterns = [
    path(f'{ENV("ADDITIONAL_URL")}ws/', URLRouter(game_ws_urlpatterns))
]
