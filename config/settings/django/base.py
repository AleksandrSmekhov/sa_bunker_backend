import environ
import os
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent.parent.parent

ENV = environ.Env(
    DEBUG=(bool, False),
    ADDITIONAL_URL=(str),
    DJANGO_SECRET_KEY=(str),
    DJANGO_ALLOWED_HOSTS=(list, []),
    PG_NAME=(str),
    PG_USER=(str),
    PG_PASSWORD=(str),
    PG_HOST=(str),
    PG_PORT=(str),
    REDIS_HOST=(str),
    REDIS_PORT=(int),
    REDIS_USER=(str),
    REDIS_PASSWORD=(str),
)

ENV.read_env(os.path.join(BASE_DIR, '.env'))


SECRET_KEY = ENV('DJANGO_SECRET_KEY')
DEBUG = ENV('DEBUG')
ALLOWED_HOSTS = ENV('DJANGO_ALLOWED_HOSTS')


ROOT_URLCONF = 'config.urls'
STATIC_URL = f'{ENV("ADDITIONAL_URL")}static/'
STATIC_ROOT = f'{BASE_DIR}\static'

LANGUAGE_CODE = 'ru-RU'
TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_TZ = True
