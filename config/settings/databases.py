from .django import ENV

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': ENV('PG_NAME'),
        'USER': ENV('PG_USER'),
        'PASSWORD': ENV('PG_PASSWORD'),
        'HOST': ENV('PG_HOST'),
        'PORT': ENV('PG_PORT'),
    }
}

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            'hosts': [(f'redis://{ENV("REDIS_USER")}:{ENV("REDIS_PASSWORD")}@{ENV("REDIS_HOST")}:{ENV("REDIS_PORT")}/{ENV("REDIS_LAYERS_DB")}')]
        }
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.redis.RedisCache',
        'LOCATION': f'redis://{ENV("REDIS_USER")}:{ENV("REDIS_PASSWORD")}@{ENV("REDIS_HOST")}:{ENV("REDIS_PORT")}/{ENV("REDIS_CACHE_DB")}',
        'TIMEOUT': 60 * 60
    }
}

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
