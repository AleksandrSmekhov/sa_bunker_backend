import os

from django.core.asgi import get_asgi_application
from channels.routing import ProtocolTypeRouter, URLRouter

from config.routing import ws_urlpatterns
from game.auth import CustomWebsocketAuthMiddle

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')

application = ProtocolTypeRouter({
    'http': get_asgi_application(),
    'websocket': CustomWebsocketAuthMiddle(URLRouter(ws_urlpatterns))
})
