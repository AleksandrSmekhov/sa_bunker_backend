from django.contrib import admin
from django.urls import path
from django.conf.urls import include

from config.settings import ENV

urlpatterns = [
    path(f'{ENV("ADDITIONAL_URL")}admin/', admin.site.urls),
    path(f'{ENV("ADDITIONAL_URL")}api/', include("game.urls"))
]
