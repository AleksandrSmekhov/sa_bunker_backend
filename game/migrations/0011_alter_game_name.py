# Generated by Django 5.0.6 on 2024-06-04 17:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0010_rename_playercard_playeringamecard_player_card'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='name',
            field=models.CharField(max_length=50, verbose_name='Имя'),
        ),
    ]
