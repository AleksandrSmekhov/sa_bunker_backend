from django.contrib import admin
from .models import GameCard, GamePlayerCardsInteractions, PlayerCard, Game, Player, PlayerGame, GameRisk, PlayerInGameCard

admin.site.register(GameCard)
admin.site.register(GamePlayerCardsInteractions)
admin.site.register(PlayerCard)
admin.site.register(Game)
admin.site.register(GameRisk)
admin.site.register(Player)
admin.site.register(PlayerGame)
admin.site.register(PlayerInGameCard)
