from django.urls import path
from .consumers import CardConsumer, PlayersConsumer, GamesConsumer, GameConsumer

ws_urlpatterns = [
    path("card/", CardConsumer.as_asgi()),
    path("player/", PlayersConsumer.as_asgi()),
    path("games/", GamesConsumer.as_asgi()),
    path("game/<uuid:id>/", GameConsumer.as_asgi())
]
