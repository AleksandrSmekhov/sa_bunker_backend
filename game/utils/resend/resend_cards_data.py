from asgiref.sync import async_to_sync
from django.core.cache import cache
from channels.layers import get_channel_layer

from game.utils.cards import get_game_cards_array, get_player_cards_array
from game.consumers import CardConsumer
from game.models import GameCard, PlayerCard


def resend_cards_data():
    cache.delete(CardConsumer.ALL_CARDS_CACHE_NAME)

    game_cards = {
        type.lower(): async_to_sync(get_game_cards_array)(type)
        for type in GameCard.TYPES}
    player_cards = {
        type.lower(): async_to_sync(get_player_cards_array)(type)
        for type in PlayerCard.TYPES}

    cards = {
        "game_cards": game_cards,
        "player_cards": player_cards
    }

    cache.set(CardConsumer.ALL_CARDS_CACHE_NAME, cards)

    channel_layer = get_channel_layer()

    async_to_sync(channel_layer.group_send)(CardConsumer.CARDS_GROUP_NAME, {
        CardConsumer.TYPE_KEY: CardConsumer.SEND_NEW_DATA_FUNCTION_NAME})
