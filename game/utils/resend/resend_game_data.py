from uuid import UUID
from asgiref.sync import async_to_sync
from django.core.cache import cache
from channels.layers import get_channel_layer

from game.utils.game.get_game_from_db import get_game_from_db
from game.consumers import GameConsumer


def resend_game_data(game_id: UUID):
    game_data = cache.get(f'{GameConsumer.GAME_CACHE_NAME}{game_id}')

    if not game_data:
        game_data = async_to_sync(get_game_from_db)(str(game_id))
        cache.set(f'{GameConsumer.GAME_CACHE_NAME}{game_id}', game_data)

    channel_layer = get_channel_layer()

    async_to_sync(channel_layer.group_send)(f'{GameConsumer.GAME_GROUP_NAME}{game_id}', {
        GameConsumer.TYPE_KEY: GameConsumer.SEND_NEW_DATA_FUNCTION_NAME,
        GameConsumer.GAME_ID_KEY: str(game_id)})
