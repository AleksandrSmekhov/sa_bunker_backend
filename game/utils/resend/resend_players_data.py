from asgiref.sync import async_to_sync
from django.core.cache import cache
from channels.layers import get_channel_layer

from game.utils.players import get_players
from game.consumers import PlayersConsumer


def resend_players_data():
    cache.delete(PlayersConsumer.ALL_PLAYERS_CACHE_NAME)

    players = async_to_sync(get_players)()
    cache.set(PlayersConsumer.ALL_PLAYERS_CACHE_NAME, players)

    channel_layer = get_channel_layer()

    async_to_sync(channel_layer.group_send)(PlayersConsumer.PLAYERS_GROUP_NAME, {
        PlayersConsumer.TYPE_KEY: PlayersConsumer.SEND_NEW_DATA_FUNCTION_NAME})
