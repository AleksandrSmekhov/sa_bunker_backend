from django.db.models.functions import Cast
from django.db.models.manager import BaseManager
from django.db.models import TextField
from channels.db import database_sync_to_async

from game.models import GameCard, PlayerCard, GamePlayerCardsInteractions


def get_full_info_array(card_manager: BaseManager[GameCard]):
    return [{GameCard.ID_FIELD: card.str_id,
             GameCard.NAME_FIELD: card.name,
             GameCard.TYPE_FIELD: card.type,
             GameCard.DESCRIPTION_FIELD: card.description,
             'positive':
             [{
                 GameCard.GAME_LINK_CARD_FIELD: player_card.str_link_id,
                 PlayerCard.ID_FIELD: player_card.str_id,
                 PlayerCard.NAME_FIELD: player_card.player_card.name,
                 PlayerCard.DESCRIPTION_FIELD: player_card.player_card.description,
                 PlayerCard.TYPE_FIELD: player_card.player_card.type
             } for player_card in GamePlayerCardsInteractions.objects.filter(
                 game_card=card.id,
                 type=GamePlayerCardsInteractions.POSITIVE_TYPE
             ).select_related(GamePlayerCardsInteractions.PLAYER_CARD_FIELD).annotate(
                 str_id=Cast(
                     f'{GamePlayerCardsInteractions.PLAYER_CARD_FIELD}__{PlayerCard.ID_FIELD}', output_field=TextField()),
                 str_link_id=Cast(
                     GamePlayerCardsInteractions.ID_FIELD, output_field=TextField())
             ).order_by(f'{GamePlayerCardsInteractions.PLAYER_CARD_FIELD}__{PlayerCard.NAME_FIELD}')],
             'negative': [{
                 GameCard.GAME_LINK_CARD_FIELD: player_card.str_link_id,
                 PlayerCard.ID_FIELD: player_card.str_id,
                 PlayerCard.NAME_FIELD: player_card.player_card.name,
                 PlayerCard.DESCRIPTION_FIELD: player_card.player_card.description,
                 PlayerCard.TYPE_FIELD: player_card.player_card.type
             } for player_card in GamePlayerCardsInteractions.objects.filter(
                 game_card=card.id,
                 type=GamePlayerCardsInteractions.NEGATIVE_TYPE
             ).select_related(GamePlayerCardsInteractions.PLAYER_CARD_FIELD).annotate(
                 str_id=Cast(
                     f'{GamePlayerCardsInteractions.PLAYER_CARD_FIELD}__{PlayerCard.ID_FIELD}', output_field=TextField()),
                 str_link_id=Cast(
                     GamePlayerCardsInteractions.ID_FIELD, output_field=TextField())
             ).order_by(f'{GamePlayerCardsInteractions.PLAYER_CARD_FIELD}__{PlayerCard.NAME_FIELD}')]
             } for card in card_manager]


@database_sync_to_async
def get_game_cards_array(type: str):
    cards = GameCard.objects.filter(type=type).annotate(
        str_id=Cast('id', output_field=TextField()),
    ).order_by(GameCard.NAME_FIELD)

    return get_full_info_array(cards)
