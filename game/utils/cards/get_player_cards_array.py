from django.db.models.functions import Cast
from django.db.models.manager import BaseManager
from django.db.models import TextField
from channels.db import database_sync_to_async

from game.models import GameCard, PlayerCard, GamePlayerCardsInteractions


def get_full_info_array(card_manager: BaseManager[PlayerCard]):
    return [{PlayerCard.ID_FIELD: card.str_id,
             PlayerCard.NAME_FIELD: card.name,
             PlayerCard.TYPE_FIELD: card.type,
             PlayerCard.DESCRIPTION_FIELD: card.description,
             'positive':
             [{
                 PlayerCard.PLAYER_LINK_CARD_FIELD: game_card.str_link_id,
                 GameCard.ID_FIELD: game_card.str_id,
                 GameCard.NAME_FIELD: game_card.game_card.name,
                 GameCard.DESCRIPTION_FIELD: game_card.game_card.description,
                 GameCard.TYPE_FIELD: game_card.game_card.type
             } for game_card in GamePlayerCardsInteractions.objects.filter(
                 player_card=card.id,
                 type=GamePlayerCardsInteractions.POSITIVE_TYPE
             ).select_related(GamePlayerCardsInteractions.GAME_CARD_FIELD).annotate(
                 str_id=Cast(
                     f'{GamePlayerCardsInteractions.GAME_CARD_FIELD}__{GameCard.ID_FIELD}', output_field=TextField()),
                 str_link_id=Cast(
                     GamePlayerCardsInteractions.ID_FIELD, output_field=TextField())
             ).order_by(f'{GamePlayerCardsInteractions.PLAYER_CARD_FIELD}__{GameCard.NAME_FIELD}')],
             'negative': [{
                 PlayerCard.PLAYER_LINK_CARD_FIELD: game_card.str_link_id,
                 GameCard.ID_FIELD: game_card.str_id,
                 GameCard.NAME_FIELD: game_card.game_card.name,
                 GameCard.DESCRIPTION_FIELD: game_card.game_card.description,
                 GameCard.TYPE_FIELD: game_card.game_card.type
             } for game_card in GamePlayerCardsInteractions.objects.filter(
                 player_card=card.id,
                 type=GamePlayerCardsInteractions.NEGATIVE_TYPE
             ).select_related(GamePlayerCardsInteractions.GAME_CARD_FIELD).annotate(
                 str_id=Cast(
                     f'{GamePlayerCardsInteractions.GAME_CARD_FIELD}__{GameCard.ID_FIELD}', output_field=TextField()),
                 str_link_id=Cast(
                     GamePlayerCardsInteractions.ID_FIELD, output_field=TextField())
             ).order_by(f'{GamePlayerCardsInteractions.PLAYER_CARD_FIELD}__{GameCard.NAME_FIELD}')]
             } for card in card_manager]


@database_sync_to_async
def get_player_cards_array(type: str):
    cards = PlayerCard.objects.filter(type=type).annotate(
        str_id=Cast('id', output_field=TextField()),
    ).order_by(PlayerCard.NAME_FIELD)

    return get_full_info_array(cards)
