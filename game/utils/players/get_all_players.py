from django.db.models.functions import Cast
from django.db.models import TextField, Count
from channels.db import database_sync_to_async

from game.models import Player


@database_sync_to_async
def get_players():
    players = Player.objects.filter(is_admin=False).annotate(str_id=Cast(
        Player.ID_FIELD, output_field=TextField()), game_amount=Count(Player.PLAYER_IN_GAME_FIELD))

    result = [{Player.ID_FIELD: player.str_id, Player.NAME_FIELD: player.name,
               "games": player.game_amount} for player in players]

    return result
