def is_next_round(round: int, players_amount: int):
    if round == 6:
        return False
    if round == 5:
        return players_amount in [4, 6]
    if round == 4 or round == 2:
        return players_amount < 6
    if round == 3:
        return players_amount in [6, 7, 8]

    return players_amount < 11
