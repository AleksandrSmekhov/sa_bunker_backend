from django.core.cache import cache

from .get_game_from_db import get_game_from_db
from .classes import GameData


async def get_game(id: str, cache_name: str, player_id: str):
    game_data: GameData = await cache.aget(cache_name)

    if not game_data:
        try:
            game_data = await get_game_from_db(id)
        except:
            return True, None

        await cache.aset(cache_name, game_data)

    try:
        return False, game_data.get_data(player_id)
    except:
        return True, None
