from django.db.models.query import Prefetch
from channels.db import database_sync_to_async

from game.models import Game, GameRisk, PlayerGame, PlayerInGameCard, PlayerCard, GamePlayerCardsInteractions, GameCard
from .classes import Disaster, GameInfo, Risk, PlayerInfo, Interaction, PlayerCardInfo, GameData


@database_sync_to_async
def get_game_from_db(id: str):
    # Запросы в базу данных #
    game = Game.objects.select_related(Game.DISASTER_FIELD).prefetch_related(Prefetch(
        Game.RISK_RELATERD_NAME, queryset=GameRisk.objects.select_related(
            GameRisk.RISK_FIELD).order_by(f'{GameRisk.RISK_FIELD}__{GameCard.TYPE_FIELD}', f'{GameRisk.RISK_FIELD}__{GameCard.NAME_FIELD}')
    ), (Prefetch(
        Game.GAME_PLAYERS_FILED, queryset=PlayerGame.objects.select_related(PlayerGame.PLAYER_FIELD).prefetch_related(Prefetch(
            PlayerGame.PLAYER_GAME_CARD_RELATED_NAME, queryset=PlayerInGameCard.objects.select_related(
                PlayerInGameCard.PLAYER_CARD_FIELD).prefetch_related(Prefetch(
                    f'{PlayerInGameCard.PLAYER_CARD_FIELD}__{PlayerCard.PLAYER_LINK_CARD_FIELD}',
                    queryset=GamePlayerCardsInteractions.objects.select_related(
                        GamePlayerCardsInteractions.GAME_CARD_FIELD)
                ))
        ))
    ))).get(pk=id)

    # Катастрофа #
    disaster = Disaster(game.disaster.id, game.disaster.name,
                        game.disaster.description) if game.disaster else None

    # Получение рисков #
    risks: list[GameRisk] = game.risks.all()
    result_risks = {str(risk.risk.id): Risk(
        str(risk.id),
        risk.is_open,
        risk.risk.name,
        risk.risk.description,
        risk.risk.type
    ) for risk in risks}

    # Получение игроков #
    players: list[PlayerGame] = game.game_players.all()
    result_players: dict[str, PlayerInfo] = {}

    for player in players:
        # Получение карт игрока #
        player_cards: list[PlayerInGameCard] = player.player_in_game_card.all()
        result_player_cards: dict[str, PlayerCardInfo] = {}

        for card in player_cards:
            # Получение взаимодействия карт #
            interactions = []
            card_interactions: list[GamePlayerCardsInteractions] = card.player_card.player_link_card.all(
            )

            for interaction in card_interactions:
                card_id = str(interaction.game_card.id)
                if card_id in result_risks.keys():
                    interactions.append(Interaction(interaction.type,
                                                    result_risks[card_id]))
                elif disaster and card_id == disaster.id:
                    interactions.append(Interaction(interaction.type,
                                                    disaster))
            # Запись карты #
            result_player_cards[str(card.id)] = PlayerCardInfo(
                card.id, card.is_open, card.player_card.name, card.player_card.description, card.player_card.type, interactions)

        # Запись игрока #
        result_players[str(player.player.id)] = PlayerInfo(player.id, player.player.name, player.vote,
                                                           player.is_ready, player.is_in_game, player.number, result_player_cards)

    # Общая информация об игре #
    game_info = GameInfo(game.id, game.name, game.current_player,
                         disaster, game.is_voting, game.round)

    return GameData(game_info, result_risks, result_players)
