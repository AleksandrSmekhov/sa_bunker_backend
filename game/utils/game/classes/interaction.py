from .risk import Risk
from .disaster import Disaster


class Interaction:
    def __init__(self, type: str, game_card: Risk | Disaster):
        self.type = type
        self.game_card = game_card

    def get_data(self):
        return {
            "type": self.type,
            "game_card": self.game_card.name if isinstance(self.game_card, Disaster) else self.game_card.name if self.game_card.is_open else None
        }
