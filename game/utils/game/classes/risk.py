from uuid import UUID


class Risk:
    def __init__(self, id: UUID, is_open: bool, name: str, description: str, type: str):
        self.id = str(id)
        self.is_open = is_open
        self.name = name
        self.description = description
        self.type = type

    def get_data(self):
        return {
            "id": self.id,
            "is_open": self.is_open,
            "type": self.type,
            "name": self.name if self.is_open else None,
            "description": self.description if self.is_open else None
        }
