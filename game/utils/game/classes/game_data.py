from .game_info import GameInfo
from .risk import Risk
from .player_info import PlayerInfo


class GameData:
    def __init__(self, game: GameInfo, risks: dict[str, Risk], players: dict[str, PlayerInfo]):
        self.game = game
        self.risks = risks
        self.players = players

    def get_data(self, player_id: str):
        return {
            "game": self.game.get_data(),
            "risks": [self.risks[risk_key].get_data() for risk_key in self.risks.keys()],
            "players": sorted([
                self.players[player_key].get_only_open(
                ) if self.players[player_key].is_in_game else self.players[player_key].get_all_data()
                for player_key in self.players.keys()
                if player_key != player_id
            ], key=lambda player: player["number"]),
            "player": self.players[player_id].get_all_data()
        }
