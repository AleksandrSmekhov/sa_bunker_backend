from uuid import UUID


class Disaster:
    def __init__(self, id: UUID, name: str, description: str):
        self.id = str(id)
        self.name = name
        self.description = description

    def get_data(self):
        return {
            "id": self.id,
            "name": self.name,
            "description": self.description
        }
