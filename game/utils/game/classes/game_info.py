from uuid import UUID

from .disaster import Disaster


class GameInfo:
    def __init__(self, id: UUID, name: str, current_player: int, disaster: Disaster, is_voting: bool, round: int):
        self.id = str(id)
        self.name = name
        self.current_player = current_player
        self.disaster = disaster
        self.is_voting = is_voting
        self.round = round

    def get_data(self):
        return {
            "id": self.id,
            "name": self.name,
            "current_player": self.current_player,
            "is_voting": self.is_voting,
            "round": self.round,
            "disaster": self.disaster.get_data() if self.disaster else None
        }
