from uuid import UUID

from .player_card_info import PlayerCardInfo


class PlayerInfo:
    def __init__(self, id: UUID, name: str, vote: UUID, is_ready: bool, is_in_game: bool, number: int, cards: dict[str, PlayerCardInfo]):
        self.id = str(id)
        self.name = name
        self.vote = str(vote) if vote else None
        self.is_ready = is_ready
        self.is_in_game = is_in_game
        self.number = number
        self.cards = cards if len(cards.keys()) else None

    def get_all_data(self):
        return {
            "id": self.id,
            "name": self.name,
            "vote": self.vote,
            "is_ready": self.is_ready,
            "is_in_game": self.is_in_game,
            "number": self.number,
            "cards": [self.cards[card_key].get_all_data() for card_key in self.cards.keys()] if self.cards else []
        }

    def get_only_open(self):
        return {
            "id": self.id,
            "name": self.name,
            "vote": self.vote,
            "is_ready": self.is_ready,
            "is_in_game": self.is_in_game,
            "number": self.number,
            "cards": [self.cards[card_key].get_only_open() for card_key in self.cards.keys()] if self.cards else []
        }
