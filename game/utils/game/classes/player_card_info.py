from uuid import UUID

from .interaction import Interaction


class PlayerCardInfo:
    def __init__(self, id: UUID, is_open: bool, name: str, description: str, type: str, interactions: list[Interaction]):
        self.id = str(id)
        self.is_open = is_open
        self.name = name
        self.description = description
        self.type = type
        self.interactions = interactions

    def get_all_data(self):
        return {
            "id": self.id,
            "is_open": self.is_open,
            "name": self.name,
            "description": self.description,
            "type": self.type,
            "interactions": [interaction.get_data() for interaction in self.interactions]
        }

    def get_only_open(self):
        return {
            "id": self.id,
            "is_open": self.is_open,
            "name": self.name if self.is_open else None,
            "description": self.description if self.is_open else None,
            "type": self.type,
            "interactions": [interaction.get_data() for interaction in self.interactions] if self.is_open else []
        }
