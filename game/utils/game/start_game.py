import random

from django.db import transaction

from game.models import PlayerGame, PlayerCard, GameCard, GameRisk, PlayerInGameCard, Game


def start_game(players: list[PlayerGame], game: Game):
    players_amount = len(players)

    with transaction.atomic():
        health_cards = random.sample(list(PlayerCard.objects.filter(
            type=PlayerCard.HEALTH_TYPE)), players_amount)
        hobby_cards = random.sample(list(PlayerCard.objects.filter(
            type=PlayerCard.HOBBY_TYPE)), players_amount)
        baggage_cards = random.sample(list(PlayerCard.objects.filter(
            type=PlayerCard.BAGGAGE_TYPE)), players_amount)
        biology_cards = random.sample(list(PlayerCard.objects.filter(
            type=PlayerCard.BIOLOGY_TYPE)), players_amount)
        facts_cards = random.sample(list(PlayerCard.objects.filter(
            type=PlayerCard.FACT_TYPE)), players_amount)
        proffesions_cards = random.sample(list(PlayerCard.objects.filter(
            type=PlayerCard.PROFESSION_TYPE)), players_amount)

        bunker_cards = random.sample(
            list(GameCard.objects.filter(type=GameCard.BUNKER_TYPE)), 3)
        risks_cards = random.sample(
            list(GameCard.objects.filter(type=GameCard.RISK_TYPE)), 2)
        disaster_card = random.choice(
            list(GameCard.objects.filter(type=GameCard.DISASTER_TYPE)))

        game.disaster = disaster_card
        game.round = 1

        game.save()

        GameRisk.objects.bulk_create([
            *[GameRisk(game=game, risk=risk)
              for risk in risks_cards],
            *[GameRisk(game=game, risk=bunker) for bunker in bunker_cards]
        ])

        players_cards = []

        for i in range(players_amount):
            players_cards.append(
                PlayerInGameCard(player=players[i], player_card=health_cards[i]))
            players_cards.append(
                PlayerInGameCard(player=players[i], player_card=hobby_cards[i]))
            players_cards.append(
                PlayerInGameCard(player=players[i], player_card=baggage_cards[i]))
            players_cards.append(
                PlayerInGameCard(player=players[i], player_card=biology_cards[i]))
            players_cards.append(
                PlayerInGameCard(player=players[i], player_card=proffesions_cards[i]))
            players_cards.append(
                PlayerInGameCard(player=players[i], player_card=facts_cards[i]))

        PlayerInGameCard.objects.bulk_create(players_cards)
