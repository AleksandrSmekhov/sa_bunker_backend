from uuid import UUID

from game.models import PlayerGame
from game.const import KEYS, MESSAGES


def validate_vote_data(user_id: UUID, data: dict[str, str]) -> tuple[PlayerGame | None, dict[str, list[str]]]:
    errors = {
        KEYS.PLAYER_ID_KEY: [],
        KEYS.GAME_ID_KEY: [],
        KEYS.VOTE_KEY: [],
    }

    try:
        player = PlayerGame.objects.select_related(
            PlayerGame.PLAYER_FIELD, PlayerGame.GAME_FIELD).get(pk=data[KEYS.PLAYER_ID_KEY])
    except:
        errors[KEYS.PLAYER_ID_KEY].append(MESSAGES.WRONG_PLAYER_ID)
        errors.pop(KEYS.GAME_ID_KEY, KEYS.VOTE_KEY)
        return None, errors

    if str(player.game.id) != data[KEYS.GAME_ID_KEY]:
        errors[KEYS.GAME_ID_KEY].append(MESSAGES.NO_GAME_WITH_PLAYER)

    if player.player.id != user_id:
        errors[KEYS.PLAYER_ID_KEY].append(
            MESSAGES.NOT_ALLOWED_TO_VOTE_FOR_ANOTHER)

    if player.vote:
        errors[KEYS.PLAYER_ID_KEY].append(MESSAGES.ALLREADY_VOTE)

    if not player.is_in_game:
        errors[KEYS.PLAYER_ID_KEY].append(MESSAGES.NOT_ALLOWED_TO_VOTE_DEAD)

    if data[KEYS.VOTE_KEY] == data[KEYS.PLAYER_ID_KEY]:
        errors[KEYS.VOTE_KEY].append(MESSAGES.NOT_ALLOWED_SELF_VOTE)

    try:
        vote_player = PlayerGame.objects.get(pk=data[KEYS.VOTE_KEY])
        if not vote_player.is_in_game:
            errors[KEYS.VOTE_KEY].append(MESSAGES.NOT_ALLOWED_TO_VOTE_FOR_DEAD)
    except:
        errors[KEYS.VOTE_KEY].append(MESSAGES.WRONG_PLAYER_ID)

    if player.number != player.game.current_player:
        errors[KEYS.PLAYER_ID_KEY].append(MESSAGES.NOT_YOUR_TURN)

    if not player.game.is_voting or player.game.round > 6 or not player.game.round:
        errors[KEYS.GAME_ID_KEY].append(MESSAGES.NOT_ALLOWED_TO_VOTE)

    return player, {key: errors[key] for key in errors.keys() if len(errors[key])}
