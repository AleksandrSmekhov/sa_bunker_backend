from uuid import UUID

from game.models import PlayerGame, PlayerInGameCard
from game.const import KEYS, MESSAGES


def validate_open_card_data(user_id: UUID, data: dict[str, str]) -> tuple[PlayerInGameCard | None, dict[str, list[str]]]:
    errors = {
        KEYS.PLAYER_ID_KEY: [],
        KEYS.CARD_ID_KEY: [],
        KEYS.ERROR_KEY: [],
    }

    try:
        card = PlayerInGameCard.objects.select_related(
            PlayerInGameCard.PLAYER_FIELD, f'{PlayerInGameCard.PLAYER_FIELD}__{PlayerGame.PLAYER_FIELD}', f'{PlayerInGameCard.PLAYER_FIELD}__{PlayerGame.GAME_FIELD}').get(pk=data[KEYS.CARD_ID_KEY])
    except:
        errors[KEYS.CARD_ID_KEY].append(MESSAGES.WRONG_CARD_ID)
        errors.pop(KEYS.PLAYER_ID_KEY, KEYS.ERROR_KEY)
        return None, errors

    if card.is_open:
        errors[KEYS.CARD_ID_KEY].append(MESSAGES.ALLREADY_OPENED)

    if str(card.player.id) != data[KEYS.PLAYER_ID_KEY]:
        errors[KEYS.CARD_ID_KEY].append(MESSAGES.NO_PLAYER_CARD)

    if str(card.player.game.id) != data[KEYS.GAME_ID_KEY]:
        errors[KEYS.CARD_ID_KEY].append(MESSAGES.NO_GAME_CARD)

    if card.player.player.id != user_id:
        errors[KEYS.CARD_ID_KEY].append(
            MESSAGES.NOT_ALLOWED_TO_OPEN_FOREIGN_CARD)

    if card.player.number != card.player.game.current_player:
        errors[KEYS.PLAYER_ID_KEY].append(MESSAGES.NOT_YOUR_TURN)

    if card.player.game.is_voting or card.player.game.round > 6:
        errors[KEYS.ERROR_KEY].append(MESSAGES.NOT_ALLOWED_TO_OPEN_CARD)

    return card, {key: errors[key] for key in errors.keys() if len(errors[key])}
