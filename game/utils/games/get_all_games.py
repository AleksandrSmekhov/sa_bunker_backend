from django.db.models.functions import Cast
from django.db.models import TextField, Count
from channels.db import database_sync_to_async
from django.db import connection

from game.models import Game, PlayerGame, Player


def get_active_games(player_id: str):
    return Game.objects.filter(
        round__lt=7).annotate(
        str_id=Cast(Game.ID_FIELD, output_field=TextField()),
        players=Count(
            f'{Game.GAME_PLAYERS_FILED}__{PlayerGame.PLAYER_FIELD}')
    ).filter(
        game_players__player__id=player_id).select_related(Game.DISASTER_FIELD).order_by(Game.NAME_FIELD)


def get_archive_games(player_id: str):
    return Game.objects.filter(
        round=7).annotate(
        str_id=Cast(Game.ID_FIELD, output_field=TextField()),
        players=Count(
            f'{Game.GAME_PLAYERS_FILED}__{PlayerGame.PLAYER_FIELD}')
    ).filter(
        game_players__player__id=player_id).select_related(Game.DISASTER_FIELD).order_by(Game.NAME_FIELD)


def get_available_games(player_id):
    return Game.objects.filter(
        round=0).annotate(
        str_id=Cast(Game.ID_FIELD, output_field=TextField()),
        players=Count(
            f'{Game.GAME_PLAYERS_FILED}__{PlayerGame.PLAYER_FIELD}')
    ).exclude(
        game_players__player__id=player_id).order_by(Game.NAME_FIELD)


@database_sync_to_async
def get_all_games(player_id: str):
    active_games: list[Game] = get_active_games(player_id)
    archive_games: list[Game] = get_archive_games(player_id)
    available_games: list[Game] = get_available_games(player_id)

    result = {"active": [{Game.ID_FIELD: game.str_id,
                          Game.NAME_FIELD: game.name,
                          Game.ROUND_FIELD: game.round,
                          Game.IS_VOTING_FIELD: game.is_voting,
                          Game.CURRENT_PLAYER_FIELD: game.current_player,
                          "players": game.players,
                          Game.DISASTER_FIELD: game.disaster.name if game.disaster else ""}
                         for game in active_games],
              "archive": [{Game.ID_FIELD: game.str_id,
                           Game.NAME_FIELD: game.name,
                           "players": game.players,
                           Game.DISASTER_FIELD: game.disaster.name}
                          for game in archive_games],
              "avaliable": [{Game.ID_FIELD: game.str_id,
                             Game.NAME_FIELD: game.name,
                             "players": game.players,
                             Game.PASSWORD_FIELD: True if game.password else False}
                            for game in available_games]}

    return result
