import json

from channels.generic.websocket import AsyncWebsocketConsumer
from django.core.cache import cache

from game.models import GameCard, PlayerCard
from game.utils.cards import get_game_cards_array, get_player_cards_array
from game.const import KEYS
from game.auth import User


class CardConsumer (AsyncWebsocketConsumer):
    ALL_CARDS_CACHE_NAME = 'all_cards'
    CARDS_GROUP_NAME = 'cards'
    TYPE_KEY = 'type'
    SEND_NEW_DATA_FUNCTION_NAME = 'send_new_data'

    # Обработка подключения #
    async def connect(self):
        user: User = self.scope[KEYS.WEB_SOCKET_USER_KEY]
        if not user.id:
            await self.close()
            return

        await self.channel_layer.group_add(self.CARDS_GROUP_NAME, self.channel_name)
        await self.accept()

        data = await self.get_game_cards()

        await self.send(json.dumps(data, ensure_ascii=False))

    # Отключение от прослушивания #
    async def disconnect(self, _):
        await self.channel_layer.group_discard(self.CARDS_GROUP_NAME, self.channel_name)
        return

    # Отправка новых данных #
    async def send_new_data(self, _):
        result = await self.get_game_cards()
        await self.send(json.dumps(result, ensure_ascii=False))

    # Получение всех карт #
    async def get_game_cards(self):
        all_cards_cache = await cache.aget(self.ALL_CARDS_CACHE_NAME)

        if (all_cards_cache):
            return all_cards_cache

        game_cards = {
            type.lower(): await get_game_cards_array(type)
            for type in GameCard.TYPES}
        player_cards = {
            type.lower(): await get_player_cards_array(type)
            for type in PlayerCard.TYPES}

        result = {
            "game_cards": game_cards,
            "player_cards": player_cards
        }

        await cache.aset(self.ALL_CARDS_CACHE_NAME, result)

        return result
