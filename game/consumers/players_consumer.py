import json

from channels.generic.websocket import AsyncWebsocketConsumer
from django.core.cache import cache

from game.const import KEYS
from game.auth import User
from game.utils.players import get_players


class PlayersConsumer (AsyncWebsocketConsumer):
    PLAYERS_GROUP_NAME = 'players'
    ALL_PLAYERS_CACHE_NAME = 'all_players'
    TYPE_KEY = 'type'
    SEND_NEW_DATA_FUNCTION_NAME = 'send_new_data'

    # Обработка подключения #
    async def connect(self):
        user: User = self.scope[KEYS.WEB_SOCKET_USER_KEY]
        if not user.id:
            await self.close()
            return

        await self.channel_layer.group_add(self.PLAYERS_GROUP_NAME, self.channel_name)
        await self.accept()

        data = await self.get_players()

        await self.send(json.dumps(data, ensure_ascii=False))

    # Отключение от прослушивания #
    async def disconnect(self, _):
        await self.channel_layer.group_discard(self.PLAYERS_GROUP_NAME, self.channel_name)
        return

    # Отправка новых данных #
    async def send_new_data(self, _):
        result = await self.get_players()
        await self.send(json.dumps(result, ensure_ascii=False))

    # Получение всех игроков #
    async def get_players(self):
        all_players_cache = await cache.aget(self.ALL_PLAYERS_CACHE_NAME)

        if (all_players_cache):
            return all_players_cache

        players = await get_players()

        await cache.aset(self.ALL_PLAYERS_CACHE_NAME, players)

        return players
