import json

from channels.generic.websocket import AsyncWebsocketConsumer

from game.utils.games import get_all_games
from game.const import KEYS
from game.auth import User


class GamesConsumer (AsyncWebsocketConsumer):
    GAMES_GROUP_NAME = 'games'
    TYPE_KEY = 'type'
    SEND_NEW_DATA_FUNCTION_NAME = 'send_new_data'

    # Обработка подключения #
    async def connect(self):
        user: User = self.scope[KEYS.WEB_SOCKET_USER_KEY]
        if not user.id:
            await self.close()
            return

        await self.channel_layer.group_add(self.GAMES_GROUP_NAME, self.channel_name)
        await self.accept()

        data = await self.get_games()

        await self.send(json.dumps(data, ensure_ascii=False))

    # Отключение от прослушивания #
    async def disconnect(self, _):
        await self.channel_layer.group_discard(self.GAMES_GROUP_NAME, self.channel_name)
        return

    # Отправка новых данных #
    async def send_new_data(self, _):
        result = await self.get_games()
        await self.send(json.dumps(result, ensure_ascii=False))

    # Получение всех игр #
    async def get_games(self):
        user: User = self.scope[KEYS.WEB_SOCKET_USER_KEY]

        games = await get_all_games(user.id)

        return games
