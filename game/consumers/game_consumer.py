import json

from rest_framework import status
from django.core.cache import cache
from channels.generic.websocket import AsyncWebsocketConsumer

from game.utils.game import get_game
from game.const import KEYS, MESSAGES
from game.auth import User


class GameConsumer (AsyncWebsocketConsumer):
    GAME_GROUP_NAME = 'game_'
    GAME_CACHE_NAME = 'game_info_'
    TYPE_KEY = 'type'
    GAME_ID_KEY = 'game_id'
    SEND_NEW_DATA_FUNCTION_NAME = 'send_new_data'
    CLOSE_CONNECTIONS_FUNCTION_NAME = 'close_connections'

    # Обработка подключения #
    async def connect(self):
        user: User = self.scope[KEYS.WEB_SOCKET_USER_KEY]
        if not user.id:
            await self.close(code=status.HTTP_401_UNAUTHORIZED, reason=MESSAGES.NO_AUTHORIZATION_ID)
            return

        game_id = self.scope[KEYS.URL_ROUTE_KEY][KEYS.KWARGS_KEY][KEYS.ID_KEY]
        isError, data = await self.get_game(game_id)

        if (isError):
            await self.close()
            return

        await self.channel_layer.group_add(f'{self.GAME_GROUP_NAME}{str(game_id)}', self.channel_name)
        await self.accept()
        await self.send(json.dumps(data, ensure_ascii=False))

    # Отключение от прослушивания #
    async def disconnect(self, _):
        game_id = self.scope[KEYS.URL_ROUTE_KEY][KEYS.KWARGS_KEY][KEYS.ID_KEY]
        await self.channel_layer.group_discard(f'{self.GAME_GROUP_NAME}{str(game_id)}', self.channel_name)
        return

    # Отправка новых данных #
    async def send_new_data(self, event):
        isError, result = await self.get_game(event[self.GAME_ID_KEY])

        if (isError):
            await self.close()
            return

        await self.send(json.dumps(result, ensure_ascii=False))

    # Закрытие всех соединений слоя #
    async def close_connections(self, _):
        await self.close()

    # Получение информации об игре #
    async def get_game(self, game_id):
        user: User = self.scope[KEYS.WEB_SOCKET_USER_KEY]

        isError, game = await get_game(game_id, f'{self.GAME_CACHE_NAME}{game_id}', user.id)

        return isError, game
