from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.request import Request

from game.auth import CustomAuthentication, CustomAdminDeletePermission
from game.const import MESSAGES, KEYS
from game.utils.resend import resend_players_data
from game.models import Player


class PlayerView(APIView):
    authentication_classes = [CustomAuthentication]
    permission_classes = [CustomAdminDeletePermission]

    def delete(self, request: Request):
        id = request.query_params.get(KEYS.ID_KEY, None)

        if not id:
            return Response({KEYS.ERROR_KEY: MESSAGES.NO_ID_IN_QUERY}, status=status.HTTP_400_BAD_REQUEST)

        try:
            player = Player.objects.get(pk=id)
            player.delete()
        except:
            return Response({KEYS. ID_KEY: MESSAGES.WRONG_PLAYER_ID}, status=status.HTTP_400_BAD_REQUEST)

        resend_players_data()

        return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)
