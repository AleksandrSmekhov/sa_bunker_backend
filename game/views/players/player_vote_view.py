from django.core.cache import cache
from rest_framework.decorators import api_view, authentication_classes
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework import status

from game.auth import CustomAuthentication
from game.const import MESSAGES, KEYS, METHODS
from game.models import PlayerGame, Player, GameRisk
from game.serializers import VoteSerializer
from game.utils.game import GameData
from game.utils.resend import resend_game_data, resend_game_and_games_data
from game.utils.validation import validate_vote_data
from game.consumers import GameConsumer


@api_view([METHODS.PATCH_MEHOD])
@authentication_classes([CustomAuthentication])
def player_vote(request: Request):
    # Чтение данных
    data = request.data
    user: Player = request.user

    # Валидация
    serializer = VoteSerializer(data=data)
    serializer.is_valid(raise_exception=True)
    player, errors = validate_vote_data(user.id, data)
    if not player or errors:
        return Response(errors, status=status.HTTP_400_BAD_REQUEST)

    # Определение последний ли голос
    next_player = PlayerGame.objects.filter(
        game__id=player.game.id, is_in_game=True, number__gt=player.number).order_by(PlayerGame.NUMBER_FIELD).first()
    next_number = next_player and next_player.number

    # Запись голоса
    player.vote = data[KEYS.VOTE_KEY]
    player.save()

    # Чтение кэша и запись голоса в кэш
    game_data: GameData = cache.get(
        f'{GameConsumer.GAME_CACHE_NAME}{player.game.id}')
    if game_data:
        game_data.players[str(user.id)].vote = data[KEYS.VOTE_KEY]

    # Переход к следующему голосующему
    if next_number:
        # Запись нового кэша
        if game_data:
            game_data.game.current_player = next_number
            cache.set(
                f'{GameConsumer.GAME_CACHE_NAME}{player.game.id}', game_data)

        # Обновление бд
        player.game.current_player = next_number
        player.game.save()

        # Отправка данных
        resend_game_data(player.game.id)
        return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)

    # Выбор всех живых игроков
    players = list(PlayerGame.objects.select_related(PlayerGame.GAME_FIELD, PlayerGame.PLAYER_FIELD).filter(game__id=player.game.id,
                   is_in_game=True).order_by(PlayerGame.NUMBER_FIELD))

    # Подсчёт голосов
    players_votes = {}
    for single_player in players:
        players_votes[str(single_player.vote)] = players_votes[str(
            single_player.vote)] + 1 if str(single_player.vote) in players_votes.keys() else 1

    # Поиск претенденов
    selected_player_id = None
    max_votes = -1
    for vote in players_votes.keys():
        if players_votes[vote] > max_votes:
            max_votes = players_votes[vote]
            selected_player_id = vote
        elif players_votes[vote] == max_votes:
            selected_player_id = None

    # Обнуление голосов
    PlayerGame.objects.select_related(PlayerGame.GAME_FIELD).filter(game__id=player.game.id,
                                                                    is_in_game=True).update(vote=None)

    if game_data:
        for key in game_data.players:
            game_data.players[key].vote = None

    # Переголосование
    if not selected_player_id:
        # Перезапись кэша
        if game_data:
            game_data.game.current_player = players[0].number
            cache.set(
                f'{GameConsumer.GAME_CACHE_NAME}{player.game.id}', game_data)

        # Обновление бд
        player.game.current_player = players[0].number
        player.game.save()

        # Отправка данных
        resend_game_data(player.game.id)
        return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)

    # Выбор игрока на вылет
    selected_player: PlayerGame = None
    for single_player in players:
        if str(single_player.id) == selected_player_id:
            selected_player = single_player
            break

    # Пометка, что игрок выбыл
    selected_player.is_in_game = False
    selected_player.vote = None
    if game_data:
        game_data.players[str(selected_player.player.id)].is_in_game = False
    selected_player.save()

    # Переход на новый раунд или новый круг голосования
    players_amount = PlayerGame.objects.filter(
        game__id=player.game.id).count()
    is_next_vote = (players_amount in [13, 14] and len(players) == players_amount - 1) or (
        players_amount in [15, 16] and (len(players) == players_amount - 1 or len(players) == players_amount - 4))

    # Определение следующего игрока
    next_number = players[0].number if players[
        0].number != selected_player.number else players[1].number
    player.game.current_player = next_number
    if game_data:
        game_data.game.current_player = next_number

    # Переход на новый раунд
    if not is_next_vote:
        # Открытие карты угрозы
        risk_card = GameRisk.objects.filter(
            game__id=player.game.id, is_open=False).select_related(GameRisk.RISK_FIELD).first()
        if (risk_card):
            risk_card.is_open = True
            risk_card.save()

        # Переход на новый раунд
        player.game.round += 1
        player.game.is_voting = False

        # Обновление данных кэша
        if game_data:
            game_data.game.is_voting = False
            game_data.game.round += 1
            if (risk_card):
                game_data.risks[str(risk_card.risk.id)].is_open = True

    # Обновление кэша
    if game_data:
        cache.set(
            f'{GameConsumer.GAME_CACHE_NAME}{player.game.id}', game_data)

    # Обновление бд
    player.game.save()

    # Отправка данных
    if is_next_vote:
        resend_game_data(player.game.id)
    else:
        resend_game_and_games_data(player.game.id)
    return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)
