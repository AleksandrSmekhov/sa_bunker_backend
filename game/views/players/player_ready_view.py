from django.core.cache import cache
from rest_framework.decorators import api_view, authentication_classes
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework import status

from game.auth import CustomAuthentication
from game.const import MESSAGES, KEYS, METHODS
from game.models import PlayerGame, Player
from game.serializers import IsReadySerializer
from game.utils.game import GameData, start_game
from game.utils.resend import resend_game_data, resend_game_and_games_data
from game.consumers import GameConsumer


@api_view([METHODS.PATCH_MEHOD])
@authentication_classes([CustomAuthentication])
def player_ready(request: Request):
    data = request.data
    user: Player = request.user

    serializer = IsReadySerializer(data=data)
    serializer.is_valid(raise_exception=True)

    try:
        player = PlayerGame.objects.select_related(
            PlayerGame.PLAYER_FIELD, PlayerGame.GAME_FIELD).get(pk=data[KEYS.PLAYER_ID_KEY])
    except:
        return Response({KEYS.PLAYER_ID_KEY: MESSAGES.WRONG_PLAYER_ID}, status=status.HTTP_400_BAD_REQUEST)

    if str(player.game.id) != data[KEYS.GAME_ID_KEY]:
        return Response({KEYS.GAME_ID_KEY: MESSAGES.NO_GAME_WITH_PLAYER}, status=status.HTTP_400_BAD_REQUEST)
    if player.player.id != user.id:
        return Response({KEYS.PLAYER_ID_KEY: MESSAGES.NOT_ALLOWED}, status=status.HTTP_403_FORBIDDEN)

    player.is_ready = data[KEYS.IS_READY_KEY]
    player.save()

    not_ready_players = PlayerGame.objects.filter(
        game__id=player.game.id, is_ready=False).count()
    players = list(PlayerGame.objects.filter(game__id=player.game.id))
    players_amount = len(players)

    if not data[KEYS.IS_READY_KEY] or not_ready_players or players_amount < 4:
        game_data: GameData = cache.get(
            f'{GameConsumer.GAME_CACHE_NAME}{player.game.id}')

        if (game_data):
            game_data.players[str(user.id)].is_ready = data[KEYS.IS_READY_KEY]
            cache.set(
                f'{GameConsumer.GAME_CACHE_NAME}{player.game.id}', game_data)

        resend_game_data(player.game.id)

        return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)

    try:
        start_game(players, player.game)
    except:
        return Response({KEYS.ERROR_KEY: MESSAGES.SOME_PROBLEMS}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    cache.delete(f'{GameConsumer.GAME_CACHE_NAME}{player.game.id}')

    resend_game_and_games_data(player.game.id)

    return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)
