from django.core.cache import cache
from django.db.models import Count, F, Case, When, SmallIntegerField
from django.db import connection
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.request import Request

from game.auth import CustomAuthentication
from game.const import MESSAGES, KEYS
from game.models import Game, PlayerGame, Player
from game.consumers import GameConsumer
from game.utils.game import GameData, PlayerInfo
from game.utils.resend import resend_game_and_games_data, resend_players_data


class GamePlayerView(APIView):
    authentication_classes = [CustomAuthentication]

    def post(self, request: Request):
        data = request.data
        user: Player = request.user
        if not data:
            return Response({KEYS.ERROR_KEY: MESSAGES.NO_DATA}, status=status.HTTP_400_BAD_REQUEST)

        if KEYS.GAME_ID_KEY not in data.keys():
            return Response({KEYS.GAME_ID_KEY: MESSAGES.NO_FIELD}, status=status.HTTP_400_BAD_REQUEST)

        try:
            game = Game.objects.annotate(players=Count(
                Game.GAME_PLAYERS_FILED)).get(pk=data[KEYS.GAME_ID_KEY])
        except:
            return Response({KEYS.GAME_ID_KEY: MESSAGES.WRONG_GAME_ID}, status=status.HTTP_400_BAD_REQUEST)

        if game.round:
            return Response({KEYS.ERROR_KEY: MESSAGES.NOT_ALLOWED_JOIN_GAME}, status=status.HTTP_403_FORBIDDEN)

        if game.password and KEYS.PASSWORD_KEY not in data.keys():
            return Response({KEYS.PASSWORD_KEY: MESSAGES.NO_FIELD}, status=status.HTTP_400_BAD_REQUEST)

        if game.password and game.password != data[KEYS.PASSWORD_KEY]:
            return Response({KEYS.PASSWORD_KEY: MESSAGES.WRONG_PASSWORD}, status=status.HTTP_403_FORBIDDEN)
        if game.players > 15:
            return Response({KEYS.ERROR_KEY: MESSAGES.MAX_PLAYERS}, status=status.HTTP_403_FORBIDDEN)

        try:
            new_player = PlayerGame.objects.create(
                player=user, game=game, number=game.players)
        except:
            return Response({KEYS.GAME_ID_KEY: MESSAGES.ALLREADY_IN_GAME}, status=status.HTTP_400_BAD_REQUEST)

        game_data: GameData = cache.get(
            f'{GameConsumer.GAME_CACHE_NAME}{game.id}')

        if (game_data):
            game_data.players[str(user.id)] = PlayerInfo(
                new_player.id, user.name, new_player.vote, new_player.is_ready, new_player.is_in_game, new_player.number, {})
            cache.set(f'{GameConsumer.GAME_CACHE_NAME}{game.id}', game_data)

        resend_game_and_games_data(game.id)
        resend_players_data()

        return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)

    def delete(self, request: Request):
        user: Player = request.user
        id = request.query_params.get(KEYS.ID_KEY, None)

        if not id:
            return Response({KEYS.ERROR_KEY: MESSAGES.NO_ID_IN_QUERY}, status=status.HTTP_400_BAD_REQUEST)

        try:
            player = PlayerGame.objects.select_related(
                PlayerGame.GAME_FIELD).select_related(PlayerGame.PLAYER_FIELD).get(pk=id)
        except:
            return Response({KEYS. ID_KEY: MESSAGES.WRONG_PLAYER_ID}, status=status.HTTP_400_BAD_REQUEST)

        if player.game.round:
            return Response({KEYS.ERROR_KEY: MESSAGES.NOT_ALLOWED_LEAVE_GAME}, status=status.HTTP_403_FORBIDDEN)
        if player.player.id != user.id and not user.is_admin:
            return Response({KEYS.ERROR_KEY: MESSAGES.NOT_ALLOWED}, status=status.HTTP_403_FORBIDDEN)

        game_data: GameData = cache.get(
            f'{GameConsumer.GAME_CACHE_NAME}{player.game.id}')

        PlayerGame.objects.filter(game__id=player.game.id).update(number=Case(When(
            number__gt=player.number, then=F(PlayerGame.NUMBER_FIELD) - 1), default=F(PlayerGame.NUMBER_FIELD), output_field=SmallIntegerField()))

        if (game_data):
            game_data.players.pop(str(player.player.id), None)
            for key in game_data.players.keys():
                if game_data.players[key].number > player.number:
                    game_data.players[key].number -= 1
            cache.set(
                f'{GameConsumer.GAME_CACHE_NAME}{player.game.id}', game_data)

        player.delete()

        resend_game_and_games_data(player.game.id)
        resend_players_data()

        return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)
