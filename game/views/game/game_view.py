from asgiref.sync import async_to_sync
from django.core.cache import cache
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.request import Request
from channels.layers import get_channel_layer

from game.auth import CustomAuthentication, CustomAdminDeletePermission
from game.const import MESSAGES, KEYS
from game.models import Game
from game.consumers import GameConsumer, GamesConsumer
from game.serializers import GameSerializer


class GameView(APIView):
    authentication_classes = [CustomAuthentication]
    permission_classes = [CustomAdminDeletePermission]

    def post(self, request: Request):
        data = request.data
        if not data:
            return Response({KEYS.ERROR_KEY: MESSAGES.NO_DATA}, status=status.HTTP_400_BAD_REQUEST)

        if KEYS.PASSWORD_KEY in data.keys() and not data[KEYS.PASSWORD_KEY]:
            data.pop(KEYS.PASSWORD_KEY)

        game = GameSerializer(data=data)
        game.is_valid(raise_exception=True)
        game.save()

        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(GamesConsumer.GAMES_GROUP_NAME, {
            GamesConsumer.TYPE_KEY: GameConsumer.SEND_NEW_DATA_FUNCTION_NAME})

        return Response(game.data, status=status.HTTP_200_OK)

    def delete(self, request: Request):
        id = request.query_params.get(KEYS.ID_KEY, None)

        if not id:
            return Response({KEYS.ERROR_KEY: MESSAGES.NO_ID_IN_QUERY}, status=status.HTTP_400_BAD_REQUEST)

        try:
            game = Game.objects.get(pk=id)
            if game.round:
                return Response({KEYS.ERROR_KEY: MESSAGES.NOT_ALLOWED_STARTED_GAME}, status=status.HTTP_403_FORBIDDEN)
            game.delete()
            channel_layer = get_channel_layer()

            async_to_sync(channel_layer.group_send)(f'{GameConsumer.GAME_GROUP_NAME}{id}', {
                GameConsumer.TYPE_KEY: GameConsumer.CLOSE_CONNECTIONS_FUNCTION_NAME})

            async_to_sync(channel_layer.group_send)(GamesConsumer.GAMES_GROUP_NAME, {
                GamesConsumer.TYPE_KEY: GameConsumer.SEND_NEW_DATA_FUNCTION_NAME})
            cache.delete(f'{GameConsumer.GAME_CACHE_NAME}{id}')
        except:
            return Response({KEYS. ID_KEY: MESSAGES.WRONG_GAME_ID}, status=status.HTTP_400_BAD_REQUEST)

        return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)
