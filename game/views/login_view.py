from rest_framework.decorators import api_view
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework import status

from game.const import MESSAGES, KEYS, METHODS
from game.models import Player
from game.utils.resend import resend_players_data


@api_view([METHODS.POST_METHOD])
def login(request: Request):
    data = request.data

    if not data:
        return Response({KEYS.ERROR_KEY: MESSAGES.NO_DATA}, status=status.HTTP_400_BAD_REQUEST)

    if KEYS.LOGIN_KEY not in request.data:
        return Response({KEYS.LOGIN_KEY: MESSAGES.NO_FIELD}, status=status.HTTP_400_BAD_REQUEST)

    name = data[KEYS.LOGIN_KEY]

    if not isinstance(name, str):
        return Response({KEYS.LOGIN_KEY: MESSAGES.WRONG_FIELD_VALUE_FORMAT}, status=status.HTTP_400_BAD_REQUEST)

    if not name:
        return Response({KEYS.LOGIN_KEY: MESSAGES.NO_FIELD}, status=status.HTTP_400_BAD_REQUEST)

    try:
        user = Player.objects.get(name=name)
        return Response({Player.ID_FIELD: user.id, Player.NAME_FIELD: user.name, Player.IS_ADMIN_FIELD: user.is_admin}, status=status.HTTP_200_OK)
    except Player.DoesNotExist:
        new_user = Player(name=name,
                          is_admin=False)
        new_user.save()
        resend_players_data()
        return Response({Player.ID_FIELD: new_user.id, Player.NAME_FIELD: new_user.name, Player.IS_ADMIN_FIELD: new_user.is_admin}, status=status.HTTP_200_OK)
