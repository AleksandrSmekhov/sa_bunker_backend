from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.request import Request

from game.auth import CustomIsAdminPermission, CustomAuthentication
from game.const import MESSAGES, KEYS
from game.serializers import CardsInteractionSerializer
from game.utils.resend import resend_cards_data
from game.models import GamePlayerCardsInteractions


class CardsInteractionView(APIView):
    authentication_classes = [CustomAuthentication]
    permission_classes = [CustomIsAdminPermission]

    def post(self, request: Request):
        data = request.data

        if not data:
            return Response({KEYS.ERROR_KEY: MESSAGES.NO_DATA}, status=status.HTTP_400_BAD_REQUEST)

        cards_interaction = CardsInteractionSerializer(data=data)
        cards_interaction.is_valid(raise_exception=True)
        cards_interaction.save()

        resend_cards_data()

        return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)

    def delete(self, request: Request):
        id = request.query_params.get(KEYS.ID_KEY, None)

        if not id:
            return Response({KEYS.ERROR_KEY: MESSAGES.NO_ID_IN_QUERY}, status=status.HTTP_400_BAD_REQUEST)

        try:
            cards_interaction = GamePlayerCardsInteractions.objects.get(pk=id)
            cards_interaction.delete()
        except:
            return Response({KEYS.ID_KEY: MESSAGES.WRONG_CARD_ID}, status=status.HTTP_400_BAD_REQUEST)

        resend_cards_data()

        return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)
