from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.request import Request

from game.auth import CustomIsAdminPermission, CustomAuthentication
from game.const import MESSAGES, KEYS
from game.serializers import PlayerCardSerializer
from game.utils.resend import resend_cards_data
from game.models import PlayerCard, GameCard


class PlayerCardView(APIView):
    authentication_classes = [CustomAuthentication]
    permission_classes = [CustomIsAdminPermission]

    def get(self, request: Request):
        if KEYS.GAME_CARD_ID_KEY not in request.query_params:
            return Response({KEYS.GAME_CARD_ID_KEY: MESSAGES.NO_FIELD}, status=status.HTTP_400_BAD_REQUEST)

        id = request.query_params[KEYS.GAME_CARD_ID_KEY]

        try:
            game_card = GameCard.objects.get(pk=id)
        except:
            return Response({KEYS.ID_KEY: MESSAGES.WRONG_CARD_ID}, status=status.HTTP_400_BAD_REQUEST)

        player_cards = PlayerCard.objects.prefetch_related(PlayerCard.PLAYER_LINK_CARD_FIELD).exclude(
            player_link_card__game_card=game_card).order_by(PlayerCard.NAME_FIELD)
        result = PlayerCardSerializer(player_cards, many=True).data

        return Response(result)

    def post(self, request: Request):
        data = request.data

        player_card = PlayerCardSerializer(data=data)
        player_card.is_valid(raise_exception=True)
        player_card.save()

        resend_cards_data()

        return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)

    def put(self, request: Request):
        data = request.data

        if not data:
            return Response({KEYS.ERROR_KEY: MESSAGES.NO_DATA}, status=status.HTTP_400_BAD_REQUEST)

        if KEYS.ID_KEY not in data:
            return Response({KEYS.ID_KEY: MESSAGES.NO_FIELD}, status=status.HTTP_400_BAD_REQUEST)

        try:
            player_card = PlayerCard.objects.get(pk=data[KEYS.ID_KEY])
        except:
            return Response({KEYS.ID_KEY: MESSAGES.WRONG_CARD_ID}, status=status.HTTP_400_BAD_REQUEST)

        serializer = PlayerCardSerializer(data=data, instance=player_card)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        resend_cards_data()

        return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)

    def delete(self, request: Request):
        id = request.query_params.get(KEYS.ID_KEY, None)

        if not id:
            return Response({KEYS.ERROR_KEY: MESSAGES.NO_ID_IN_QUERY}, status=status.HTTP_400_BAD_REQUEST)

        try:
            game_card = PlayerCard.objects.get(pk=id)
            game_card.player_link_card.all().delete()
            game_card.delete()
        except:
            return Response({KEYS.ID_KEY: MESSAGES.WRONG_CARD_ID}, status=status.HTTP_400_BAD_REQUEST)

        resend_cards_data()

        return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)
