from django.core.cache import cache
from rest_framework.decorators import api_view, authentication_classes
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework import status

from game.auth import CustomAuthentication
from game.const import MESSAGES, KEYS, METHODS
from game.models import Player, PlayerGame, GameRisk
from game.serializers import OpenCardSerializer
from game.utils.game import GameData,  is_next_round
from game.utils.validation import validate_open_card_data
from game.utils.resend import resend_game_and_games_data
from game.consumers import GameConsumer


@api_view([METHODS.PATCH_MEHOD])
@authentication_classes([CustomAuthentication])
def open_card_view(request: Request):
    # Чтение данных
    data = request.data
    user: Player = request.user

    # Валидация
    serializer = OpenCardSerializer(data=data)
    serializer.is_valid(raise_exception=True)
    card, errors = validate_open_card_data(user.id, data)
    if not card or errors:
        return Response(errors, status=status.HTTP_400_BAD_REQUEST)

    # Опредение следующего открывающего
    next_player = PlayerGame.objects.filter(
        game__id=card.player.game.id, is_in_game=True, number__gt=card.player.number).order_by(PlayerGame.NUMBER_FIELD).first()
    next_number = next_player and next_player.number

    # Открытие карты
    card.is_open = True
    card.save()

    # Чтение кэша
    game_data: GameData = cache.get(
        f'{GameConsumer.GAME_CACHE_NAME}{card.player.game.id}')
    if game_data:
        game_data.players[str(
            user.id)].cards[data[KEYS.CARD_ID_KEY]].is_open = True

    # Круг продалжается
    if next_number:
        # Обновление кэша
        if game_data:
            game_data.game.current_player = next_number
            cache.set(
                f'{GameConsumer.GAME_CACHE_NAME}{card.player.game.id}', game_data)

        # Обновление кэша
        card.player.game.current_player = next_number
        card.player.game.save()

        # Отправка данных
        resend_game_and_games_data(card.player.game.id)
        return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)

    # Следующий раунд или голосование
    players_amount = PlayerGame.objects.filter(
        game__id=card.player.game.id).count()
    is_next = is_next_round(card.player.game.round, players_amount)

    # Первый живой игрок
    next_number = PlayerGame.objects.filter(
        game__id=card.player.game.id, is_in_game=True).order_by(PlayerGame.NUMBER_FIELD).first().number
    card.player.game.current_player = next_number
    if game_data:
        game_data.game.current_player = next_number

    # Следующий раунд
    if is_next:
        # Открытие карты угрозы
        risk_card = GameRisk.objects.filter(
            game__id=card.player.game.id, is_open=False).select_related(GameRisk.RISK_FIELD).first()
        risk_card.is_open = True
        risk_card.save()

        # Переход на следующий раунд
        card.player.game.round += 1

        # Обновление кэша
        if game_data:
            game_data.game.round += 1
            game_data.risks[str(risk_card.risk.id)].is_open = True
    else:
        # Переход в режим голосования
        card.player.game.is_voting = True
        if game_data:
            game_data.game.is_voting = True

    # Сохранение данных
    if game_data:
        cache.set(
            f'{GameConsumer.GAME_CACHE_NAME}{card.player.game.id}', game_data)
    card.player.game.save()

    # Отправка данных
    resend_game_and_games_data(card.player.game.id)
    return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)
