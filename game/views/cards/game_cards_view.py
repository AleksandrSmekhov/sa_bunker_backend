from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.request import Request

from game.auth import CustomIsAdminPermission, CustomAuthentication
from game.const import MESSAGES, KEYS
from game.serializers import GameCardSerializer
from game.utils.resend import resend_cards_data
from game.models import GameCard, PlayerCard


class GameCardView(APIView):
    authentication_classes = [CustomAuthentication]
    permission_classes = [CustomIsAdminPermission]

    def get(self, request: Request):
        if KEYS.PLAYER_CARD_ID_KEY not in request.query_params:
            return Response({KEYS.PLAYER_CARD_ID_KEY: MESSAGES.NO_FIELD}, status=status.HTTP_400_BAD_REQUEST)

        id = request.query_params[KEYS.PLAYER_CARD_ID_KEY]

        try:
            player_card = PlayerCard.objects.get(pk=id)
        except:
            return Response({KEYS.ID_KEY: MESSAGES.WRONG_CARD_ID}, status=status.HTTP_400_BAD_REQUEST)

        game_cards = GameCard.objects.prefetch_related(GameCard.GAME_LINK_CARD_FIELD).exclude(
            game_link_card__player_card=player_card).order_by(GameCard.NAME_FIELD)
        result = GameCardSerializer(game_cards, many=True).data

        return Response(result)

    def post(self, request: Request):
        data = request.data

        game_card = GameCardSerializer(data=data)
        game_card.is_valid(raise_exception=True)
        game_card.save()

        resend_cards_data()

        return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)

    def put(self, request: Request):
        data = request.data

        if not data:
            return Response({KEYS.ERROR_KEY: MESSAGES.NO_DATA}, status=status.HTTP_400_BAD_REQUEST)

        if KEYS.ID_KEY not in data:
            return Response({KEYS.ID_KEY: MESSAGES.NO_FIELD}, status=status.HTTP_400_BAD_REQUEST)

        try:
            game_card = GameCard.objects.get(pk=data[KEYS.ID_KEY])
        except:
            return Response({KEYS.ID_KEY: MESSAGES.WRONG_CARD_ID}, status=status.HTTP_400_BAD_REQUEST)

        serializer = GameCardSerializer(data=data, instance=game_card)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        resend_cards_data()

        return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)

    def delete(self, request: Request):
        id = request.query_params.get(KEYS.ID_KEY, None)

        if not id:
            return Response({KEYS.ERROR_KEY: MESSAGES.NO_ID_IN_QUERY}, status=status.HTTP_400_BAD_REQUEST)

        try:
            game_card = GameCard.objects.get(pk=id)
            game_card.game_link_card.all().delete()
            game_card.delete()
        except:
            return Response({KEYS.ID_KEY: MESSAGES.WRONG_CARD_ID}, status=status.HTTP_400_BAD_REQUEST)

        resend_cards_data()

        return Response({KEYS.STATUS_KEY: MESSAGES.OK_STATUS}, status=status.HTTP_200_OK)
