import uuid
from django.db import models


class GameCard(models.Model):
    # Константы #
    RISK_TYPE = 'RISK'
    BUNKER_TYPE = 'BUNKER'
    DISASTER_TYPE = 'DISASTER'

    TYPE_CHOICES = (
        (RISK_TYPE, 'Угроза'),
        (BUNKER_TYPE, 'Бункер'),
        (DISASTER_TYPE, 'Катастрофа')
    )

    TYPES = [RISK_TYPE, BUNKER_TYPE, DISASTER_TYPE]

    # Константы полей #
    ID_FIELD = 'id'
    NAME_FIELD = 'name'
    DESCRIPTION_FIELD = 'description'
    TYPE_FIELD = 'type'
    GAME_LINK_CARD_FIELD = 'game_link_card'

    # Поля #
    id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                          editable=False, verbose_name="id")
    name = models.CharField(max_length=100, null=False,
                            verbose_name="Короткое описание")
    description = models.TextField(null=False, verbose_name="Описание")
    type = models.CharField(choices=TYPE_CHOICES, blank=False,
                            verbose_name="Тип", default=DISASTER_TYPE)

    # Доп. информация #
    class Meta:
        db_table = "GameCard"
        verbose_name = "Карточка игры"
        verbose_name_plural = "Карточки игры"

    # Отображение в django admin #
    def __str__(self):
        return f"{self.type} - {self.name}"
