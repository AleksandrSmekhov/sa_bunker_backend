import uuid
from django.db import models


class PlayerCard(models.Model):
    # Константы #
    BIOLOGY_TYPE = 'BIOLOGY'
    HEALTH_TYPE = 'HEALTH'
    HOBBY_TYPE = 'HOBBY'
    BAGGAGE_TYPE = 'BAGGAGE'
    FACT_TYPE = 'FACTS'
    PROFESSION_TYPE = 'PROFESSIONS'

    TYPE_CHOICES = (
        (BIOLOGY_TYPE, 'Биология'),
        (HEALTH_TYPE, 'Здоровье'),
        (HOBBY_TYPE, 'Хобби'),
        (BAGGAGE_TYPE, 'Багаж'),
        (FACT_TYPE, 'Факты'),
        (PROFESSION_TYPE, 'Профессия')
    )

    TYPES = [BIOLOGY_TYPE, HEALTH_TYPE, HOBBY_TYPE,
             BAGGAGE_TYPE, FACT_TYPE, PROFESSION_TYPE]

    # Константы полей #
    ID_FIELD = 'id'
    NAME_FIELD = 'name'
    DESCRIPTION_FIELD = 'description'
    TYPE_FIELD = 'type'
    PLAYER_LINK_CARD_FIELD = 'player_link_card'

    # Поля #
    id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                          editable=False, verbose_name="id")
    name = models.CharField(max_length=100, null=False,
                            verbose_name="Короткое описание")
    description = models.TextField(null=False, verbose_name="Описание")
    type = models.CharField(choices=TYPE_CHOICES, blank=False,
                            verbose_name="Тип", default=BIOLOGY_TYPE)

    # Доп. информация #
    class Meta:
        db_table = "PlayerCard"
        verbose_name = "Карточка персонажа"
        verbose_name_plural = "Карточки персонажей"

    # Отображение в django admin #
    def __str__(self):
        return f"{self.type} - {self.name}"
