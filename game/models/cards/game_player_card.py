import uuid
from django.db import models

from .game_card import GameCard
from .player_card import PlayerCard


class GamePlayerCardsInteractions(models.Model):
    # Константы #
    POSITIVE_TYPE = 'POSITIVE'
    NEGATIVE_TYPE = 'NEGATIVE'

    TYPE_CHOICES = (
        (POSITIVE_TYPE, 'Позитивная'),
        (NEGATIVE_TYPE, 'Негативная'),
    )

    TYPES = [POSITIVE_TYPE, NEGATIVE_TYPE]

    # Константы полей #
    ID_FIELD = 'id'
    PLAYER_CARD_FIELD = 'player_card'
    GAME_CARD_FIELD = 'game_card'
    TYPE_FIELD = 'type'

    # Поля #
    id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                          editable=False, verbose_name="id")
    player_card = models.ForeignKey(
        PlayerCard, on_delete=models.DO_NOTHING, verbose_name="Карты персонажей", null=False, related_name=PlayerCard.PLAYER_LINK_CARD_FIELD)
    game_card = models.ForeignKey(
        GameCard, on_delete=models.DO_NOTHING, verbose_name="Карты игры", null=False, related_name=GameCard.GAME_LINK_CARD_FIELD)
    type = models.CharField(choices=TYPE_CHOICES, blank=False,
                            verbose_name="Тип", default=POSITIVE_TYPE)

    # Доп. информация #
    class Meta:
        db_table = "GamePlayerCardsInteraction"
        verbose_name = "Взаимодействие карточек персонажей и игры"
        verbose_name_plural = "Взаимодействия карточек персонажей и игры"
        unique_together = ('player_card', 'game_card')

    # Отображение в django admin #
    def __str__(self):
        return f"{self.type} - {self.player_card.name} - ${self.game_card.name}"
