import uuid
from django.db import models

from .player_game import PlayerGame
from ..cards import PlayerCard


class PlayerInGameCard(models.Model):
    # Константы полей #
    ID_FIELD = 'id'
    PLAYER_FIELD = 'player'
    PLAYER_CARD_FIELD = 'player_card'
    IS_OPEN_FIELD = 'is_open'

    # Поля #
    id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                          editable=False, verbose_name="id")
    player = models.ForeignKey(PlayerGame, verbose_name="Игрок",
                               null=False, on_delete=models.CASCADE, related_name=PlayerGame.PLAYER_GAME_CARD_RELATED_NAME)
    player_card = models.ForeignKey(
        PlayerCard, verbose_name="Карта", null=False, on_delete=models.CASCADE)
    is_open = models.BooleanField(
        verbose_name="Карта открыта?", null=False, default=False)

    # Доп. информация #
    class Meta:
        db_table = "PlayerInGameCard"
        verbose_name = "Карточка игрока"
        verbose_name_plural = "Карточки игрока"

    # Отображение в django admin #
    def __str__(self):
        return f"{self.player.player.name} - {self.player.game.name} - {self.playerCard.type}"
