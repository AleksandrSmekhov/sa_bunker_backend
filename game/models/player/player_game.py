import uuid
from django.db import models

from ..game import Game
from .player import Player


class PlayerGame(models.Model):
    # Константы полей #
    ID_FIELD = 'id'
    PLAYER_FIELD = 'player'
    GAME_FIELD = 'game'
    NUMBER_FIELD = 'number'
    IS_READY_FIELD = 'is_ready'
    IS_IN_GAME_FIELD = 'is_in_game'
    VOTE_FIELD = 'vote'
    PLAYER_GAME_CARD_RELATED_NAME = 'player_in_game_card'

    # Поля #
    id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                          editable=False, verbose_name="id")
    player = models.ForeignKey(Player, verbose_name="Игрок",
                               null=False, on_delete=models.CASCADE, related_name=Player.PLAYER_IN_GAME_FIELD)
    game = models.ForeignKey(Game,
                             verbose_name="Игра", null=False, on_delete=models.CASCADE, related_name=Game.GAME_PLAYERS_FILED)
    is_ready = models.BooleanField(
        verbose_name="Игрок готов?", null=False, default=False)
    is_in_game = models.BooleanField(
        verbose_name="Игрок в игре?", null=False, default=True)
    vote = models.UUIDField(verbose_name="Голос за:", null=True)
    number = models.SmallIntegerField(
        verbose_name="Номер игрока в игре", null=False)

    # Доп. информация #

    class Meta:
        db_table = "PlayerGame"
        verbose_name = "Игрок в игре"
        verbose_name_plural = "Игроки в игре"
        unique_together = ('player', 'game')

    # Отображение в django admin #
    def __str__(self):
        return f"{self.player.name} - {self.game.name}"
