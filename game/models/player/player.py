import uuid
from django.db import models


class Player(models.Model):
    # Константы полей #
    ID_FIELD = 'id'
    NAME_FIELD = 'name'
    IS_ADMIN_FIELD = 'is_admin'
    PLAYER_IN_GAME_FIELD = 'in_game_player'

    # Поля #
    id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                          editable=False, verbose_name="id")
    name = models.CharField(verbose_name="Имя", null=False,
                            unique=True, editable=False)
    is_admin = models.BooleanField(
        verbose_name="Админ?", null=False, default=False)

    # Доп. информация #
    class Meta:
        db_table = "Player"
        verbose_name = "Игрок"
        verbose_name_plural = "Игроки"

    # Отображение в django admin #
    def __str__(self):
        return self.name
