import uuid
from django.db import models

from ..cards import GameCard


class Game(models.Model):
    # Константы полей #
    ID_FIELD = 'id'
    NAME_FIELD = 'name'
    ROUND_FIELD = 'round'
    DISASTER_FIELD = 'disaster'
    CURRENT_PLAYER_FIELD = 'current_player'
    PASSWORD_FIELD = 'password'
    IS_VOTING_FIELD = 'is_voting'
    GAME_PLAYERS_FILED = 'game_players'
    RISK_RELATERD_NAME = 'risks'

    # Поля #
    id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                          editable=False, verbose_name="id")
    name = models.CharField(
        max_length=50, verbose_name="Имя", null=False)
    round = models.IntegerField(verbose_name="Раунд", null=False, default=0)
    disaster = models.ForeignKey(
        GameCard, verbose_name="Катастрофа", null=True, on_delete=models.CASCADE)
    current_player = models.IntegerField(
        verbose_name="Текущий игрок", null=False, default=0)
    password = models.CharField(
        max_length=100, null=True, verbose_name="Пароль")
    is_voting = models.BooleanField(
        verbose_name="Идёт голосование?", null=False, default=False)

    # Доп. информация #
    class Meta:
        db_table = "Game"
        verbose_name = "Игра"
        verbose_name_plural = "Игры"

    # Отображение в django admin #
    def __str__(self):
        return f"{self.name} - {self.round}"
