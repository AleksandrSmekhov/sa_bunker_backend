import uuid
from django.db import models

from ..cards import GameCard
from .game import Game


class GameRisk(models.Model):
    # Константы полей #
    ID_FIELD = 'id'
    RISK_FIELD = 'risk'
    GAME_FIELD = 'game'
    IS_OPEN_FIELD = 'is_open'

    # Поля #
    id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                          editable=False, verbose_name="id")
    risk = models.ForeignKey(GameCard, verbose_name="Угроза",
                             null=False, on_delete=models.CASCADE)
    game = models.ForeignKey(Game,
                             verbose_name="Игра", null=False, on_delete=models.CASCADE, related_name=Game.RISK_RELATERD_NAME)
    is_open = models.BooleanField(
        verbose_name="Карта открыта?", null=False, default=False)

    # Доп. информация #
    class Meta:
        db_table = "GameRisk"
        verbose_name = "Угроза в игре"
        verbose_name_plural = "Угрозы в игре"

    # Отображение в django admin #
    def __str__(self):
        return f"{self.game.name} - {self.risk.name}"
