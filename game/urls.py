from django.urls import path

from .views import login
from .views import GameCardView, PlayerCardView, CardsInteractionView, open_card_view
from .views import PlayerView, player_ready, player_vote
from .views import GameView, GamePlayerView


urlpatterns = [
    path('login/', login),
    path('game_card/', GameCardView.as_view()),
    path('player_card/', PlayerCardView.as_view()),
    path('cards_interaction/', CardsInteractionView.as_view()),
    path('player/', PlayerView.as_view()),
    path('player/ready/', player_ready),
    path('player/vote/', player_vote),
    path('game/', GameView.as_view()),
    path('game_player/', GamePlayerView.as_view()),
    path('card/open/', open_card_view)
]
