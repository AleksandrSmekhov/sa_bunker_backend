from rest_framework.permissions import BasePermission, SAFE_METHODS
from rest_framework.request import Request

from game.models import Player
from game.const import KEYS


class CustomIsAdminPermission(BasePermission):
    def has_permission(self, request: Request, _):
        if request.method in SAFE_METHODS:
            return True

        user : Player = request.user

        return user.is_admin
