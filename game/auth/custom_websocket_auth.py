from channels.db import database_sync_to_async

from game.models import Player
from game.const import KEYS


class User:
    def __init__(self, id='', name='', is_admin=False):
        self.id = id
        self.name = name
        self.is_admin = is_admin


@database_sync_to_async
def returnPlayer(id: str):
    try:
        player = Player.objects.get(pk=id)
        user = User(id=id, name=player.name,
                    is_admin=player.is_admin)
    except:
        user = User()
    return user


class CustomWebsocketAuthMiddle:
    HEADER_KEY = 'headers'
    AUTH_HEADER_VALUE = b'authorization'

    def __init__(self, app):
        self.app = app

    async def __call__(self, scope, receive, send):
        user_id = scope[KEYS.QUERY_STRING_KEY].decode()

        if (user_id):
            user = await returnPlayer(user_id)
        else:
            user = User()

        scope[KEYS.WEB_SOCKET_USER_KEY] = user

        return await self.app(scope, receive, send)
