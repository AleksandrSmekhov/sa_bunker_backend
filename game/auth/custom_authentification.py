from django.db.models import TextField
from django.db.models.functions import Cast
from rest_framework.authentication import BaseAuthentication
from rest_framework.request import Request
from rest_framework.exceptions import AuthenticationFailed

from game.models import Player
from game.const import MESSAGES, KEYS, METHODS


class CustomAuthentication(BaseAuthentication):
    def authenticate(self, request: Request):
        if request.method == METHODS.OPTIONS_METHOD:
            return None

        if KEYS.AUTHORIZATION_KEY not in request.headers:
            raise AuthenticationFailed(MESSAGES.NO_AUTHORIZATION_HEADER)

        user_id = request.headers[KEYS.AUTHORIZATION_KEY]

        if not user_id:
            raise AuthenticationFailed(MESSAGES.NO_AUTHORIZATION_ID)

        try:
            user = Player.objects.annotate(str_id=Cast(
                Player.ID_FIELD, output_field=TextField())).get(pk=user_id)
        except:
            raise AuthenticationFailed(MESSAGES.WRONG_AUTHORIZATION_DATA)

        return (user, None)
