from rest_framework.serializers import ModelSerializer

from game.models import Game


class GameSerializer(ModelSerializer):
    class Meta:
        model = Game
        fields = [Game.NAME_FIELD, Game.PASSWORD_FIELD, Game.ID_FIELD]
        read_only_fields = [Game.ID_FIELD]
