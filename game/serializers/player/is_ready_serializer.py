from rest_framework import serializers


class IsReadySerializer(serializers.Serializer):
    game_id = serializers.UUIDField()
    player_id = serializers.UUIDField()
    is_ready = serializers.BooleanField()
