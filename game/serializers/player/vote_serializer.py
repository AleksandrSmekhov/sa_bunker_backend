from rest_framework import serializers


class VoteSerializer(serializers.Serializer):
    player_id = serializers.UUIDField()
    game_id = serializers.UUIDField()
    vote = serializers.UUIDField()
