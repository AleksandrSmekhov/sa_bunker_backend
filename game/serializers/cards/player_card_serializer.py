from rest_framework.serializers import ModelSerializer

from game.models import PlayerCard


class PlayerCardSerializer(ModelSerializer):
    class Meta:
        model = PlayerCard
        fields = [PlayerCard.ID_FIELD, PlayerCard.NAME_FIELD,
                  PlayerCard.DESCRIPTION_FIELD, PlayerCard.TYPE_FIELD]
        read_only_fields = [PlayerCard.ID_FIELD]
