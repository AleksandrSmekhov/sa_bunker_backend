from rest_framework.serializers import ModelSerializer

from game.models import GameCard


class GameCardSerializer(ModelSerializer):
    class Meta:
        model = GameCard
        fields = [GameCard.ID_FIELD, GameCard.NAME_FIELD,
                  GameCard.DESCRIPTION_FIELD, GameCard.TYPE_FIELD]
        read_only_fields = [GameCard.ID_FIELD]
