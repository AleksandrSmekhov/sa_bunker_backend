from rest_framework import serializers


class OpenCardSerializer(serializers.Serializer):
    card_id = serializers.UUIDField()
    player_id = serializers.UUIDField()
    game_id = serializers.UUIDField()
