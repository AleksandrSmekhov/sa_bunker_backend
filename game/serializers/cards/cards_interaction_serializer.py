from rest_framework.serializers import ModelSerializer

from game.models import GamePlayerCardsInteractions


class CardsInteractionSerializer(ModelSerializer):
    class Meta:
        model = GamePlayerCardsInteractions
        fields = [GamePlayerCardsInteractions.ID_FIELD, GamePlayerCardsInteractions.PLAYER_CARD_FIELD,
                  GamePlayerCardsInteractions.GAME_CARD_FIELD, GamePlayerCardsInteractions.TYPE_FIELD]
        read_only_fields = [GamePlayerCardsInteractions.ID_FIELD]
