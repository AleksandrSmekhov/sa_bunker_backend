FROM python:3.11-alpine

RUN mkdir -p /usr/app/
WORKDIR /usr/app/

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip
RUN pip install poetry

COPY poetry.lock pyproject.toml /usr/app/

RUN \
 apk add --no-cache postgresql-libs && \
 apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev && \
 poetry config virtualenvs.create false && \
 poetry install --only main

COPY . /usr/app/