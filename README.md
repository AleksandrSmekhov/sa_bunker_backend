# Backend pet проекта - "Игра «Бункер»­"

## .env.example

-   DEBUG - режим откладки, допустимые значения - True/False
-   DJANGO_SECRET_KEY - секретный ключ django для создания hash
-   DJANGO_ALLOWED_HOSTS - список разрешённых хостов, записывается в строку через запятую(,)
-   PG\_\* - данные для подключение к Postgres
-   REDIS\_\* - данные для подключения к Redis

## Стек

-   Django
-   Django REST Framework
-   Django channels
-   PostgreSQL
-   Redis

## Установка

Обновление pip:

### `pip install --upgrade pip`

Установка poetry:

### `pip install poetry`

Создание вирутального окружения:

### `poetry env use python[version]`

Запуск виртуального окружения:

### `poetry shell`

Установка всех пакетов:

### `poetry install`

Применение миграций:

### `python manage.py migrate`

Запуск сервера:

### `python manage.py runserver`

## REST endpoints

### Авторизация запроса

Для всех запросов, кроме запроса Авторизация/Регистрация, необходимо передать следующий заголовок:

_Authorization_ : _user_id_

### `/api/login/`

-   POST - Авторизация/Регистрация

    **Body:**

    -   name - Имя пользователя

    **Response:**

    -   id - Id пользователя
    -   name - Имя пользователя
    -   is_admin - Роль пользователя (Админ или нет)

### `/api/game_card/`

-   GET - Получение списка игровых карт, непривязанных к переданной карте персонажа

    **Query:**

    -   player_card_id - Id карты персонажа

    **Response:**

    Список карт:

    -   id - Id игровой карты
    -   name - Название игровой карты
    -   description - Описание игровой карты
    -   type - Тип игровой карты:
        -   RISK - карта угрозы
        -   BUNKER -карта бункера
        -   DISASTER - карта катастрофы

-   POST - Создание игровой карты

    **Body:**

    -   name - Название игровой карты
    -   description - Описание игровой карты
    -   type - Тип игровой карты:
        -   RISK - карта угрозы
        -   BUNKER -карта бункера
        -   DISASTER - карта катастрофы

    **Response:**

    При успешно обработке: `{ "status": "ok" }`

-   PUT - Изменение игровой карты

    **Body:**

    -   name - Название игровой карты
    -   description - Описание игровой карты
    -   type - Тип игровой карты:
        -   RISK - карта угрозы
        -   BUNKER -карта бункера
        -   DISASTER - карта катастрофы

    **Response:**

    При успешно обработке: `{ "status": "ok" }`

-   DELETE - Удаление игровой карты

    **Query:**

    -   id - Id игровой карты

    **Response:**

    При успешно обработке: `{ "status": "ok" }`

### `/api/player_card/`

-   GET - Получение списка карт персонажа, непривязанных к переданной игровой карте

    **Query:**

    -   game_card_id - Id игровой карты

    **Response:**
    Список карт:

    -   id - Id карты персонажа
    -   name - Название карты персонажа
    -   description - Описание карты персонажа
    -   type - Тип карты персонажа:
        -   BIOLOGY - карта биологии
        -   HEALTH - карта здоровья
        -   HOBBY - карта хобби
        -   BAGGAGE - карта багажа
        -   FACTS - карта фактов
        -   PROFESSIONS - карта профессии

-   POST - Создание карты персонажа

    **Body:**

    -   name - Название карты персонажа
    -   description - Описание карты персонажа
    -   type - Тип карты персонажа:
        -   BIOLOGY - карта биологии
        -   HEALTH - карта здоровья
        -   HOBBY - карта хобби
        -   BAGGAGE - карта багажа
        -   FACTS - карта фактов
        -   PROFESSIONS - карта профессии

    **Response:**

    При успешно обработке: `{ "status": "ok" }`

-   PUT - Изменение карты персонажа

    **Body:**

    -   name - Название карты персонажа
    -   description - Описание карты персонажа
    -   type - Тип карты персонажа:
        -   BIOLOGY - карта биологии
        -   HEALTH - карта здоровья
        -   HOBBY - карта хобби
        -   BAGGAGE - карта багажа
        -   FACTS - карта фактов
        -   PROFESSIONS - карта профессии

    **Response:**

    При успешно обработке: `{ "status": "ok" }`

-   DELETE - Удаление карты персонажа

    **Query:**

    -   id - Id карты персонажа

    **Response:**

    При успешно обработке: `{ "status": "ok" }`

### `/api/cards_interaction/`

-   POST - Создание привязки между игровой картой и картой персонажа

    **Body:**

    -   player_card - Id карты персонажа
    -   game_card - Id игровой карты
    -   type - тип привязки:
        -   POSITIVE - позитивное взаимодействие
        -   NEGATIVE - негативное взаимодействие

    **Response:**

    При успешно обработке: `{ "status": "ok" }`

-   DELETE - Удаление привязки

    **Query:**

    -   id - Id привязки

    **Response:**

    При успешно обработке: `{ "status": "ok" }`

### `/api/player/`

-   DELETE - Удаление пользователя

    **Query:**

    -   id - Id пользователя

    **Response:**

    При успешно обработке: `{ "status": "ok" }`

### `/api/player/ready/`

-   PATCH - Изменение готовности игрока

    **Body:**

    -   game_id - Id игры
    -   player_id - Id игрока
    -   is_ready - Готовность игров:
        -   true - готов
        -   false - не готов

    **Response:**

    При успешно обработке: `{ "status": "ok" }`

### `/api/player/vote/`

-   PATCH - Выбор игрока на вылет из игры

    **Body:**

    -   game_id - Id игры
    -   player_id - Id игрока
    -   vote - Id игрока, за которого отдаётся голос

    **Response:**

    При успешно обработке: `{ "status": "ok" }`

### `/api/game/`

-   POST - создание игры

    **Body:**

    -   name - Название игры
    -   password - Пароль (Необязательно)

    **Response:**

    -   id - Id созданной игры
    -   name - Название игры
    -   password - Пароль (Необязательно)

-   DELETE - удаление не начатой игры

    **Query:**

    -   id - Id игры

    **Response:**

    При успешно обработке: `{ "status": "ok" }`

### `api/game_player/`

-   POST - присоединение пользователя к игре (Создание игрока)

    **Body:**

    -   game_id - Id игры
    -   password - Пароль от игры (По необходимости)

    **Response:**

    При успешно обработке: `{ "status": "ok" }`

-   DELETE - выход из не начатой игры (Удаление игрока)

    **Query:**

    -   id - Id игрока

    **Response:**

    При успешно обработке: `{ "status": "ok" }`

### `api/card/open/`

-   PATCH - открытие карты

    **Body:**

    -   game_id - Id игры
    -   player_id - Id игрока
    -   card_id - Id карты

    **Response:**

    При успешно обработке: `{ "status": "ok" }`

## WS endpoints

Все вебсокеты используется для отдачи данных, всё взаимодействие с данными осуществляется с помощью REST API

### Авторизация запроса

Для всех запросов необходимо передать Id пользователя в качестве параметра:
_ws://baseUrl/Endpoint/?userId_

### `ws/card/`

**Message**

_Cards:_

```
{
    game_cards: {  // Игровые карты
        risk: [  // Список карт угроз
            GameCardInfo,
            GameCardInfo,
            ...
        ],
        bunker: [  // Список карт бункера
            GameCardInfo,
            GameCardInfo,
            ...
        ],
        disaster: [  // Список карт катастроф
            GameCardInfo,
            GameCardInfo,
            ...
        ]
    },
    player_cards: {  // Карты персонажа
        baggage: [  // Список карт багажа
            PlayerCardInfo,
            PlayerCardInfo,
            ...
        ],
        biology: [  // Список карт биологии
            PlayerCardInfo,
            PlayerCardInfo,
            ...
        ],
        facts: [  // Список карт фактов
            PlayerCardInfo,
            PlayerCardInfo,
            ...
        ],
        health: [  // Список карт здоровья
            PlayerCardInfo,
            PlayerCardInfo,
            ...
        ],
        hobby: [  // Список карт хобби
            PlayerCardInfo,
            PlayerCardInfo,
            ...
        ],
        professions: [  // Список карт профессиий
            PlayerCardInfo,
            PlayerCardInfo,
            ...
        ]
    }
}
```

_GameCardInfo:_

```
{
    id: string,  // Id игровой карты
    name: string,  // Название карты
    description: string,  // Описание карты
    type: string,  // Тип карты
    negative: [  // Список негативных взаимодействий
        GameCardInteraction,
        GameCardInteraction,
        ...
    ],
    positive: [  // Список позитивных взаимодействий
        GameCardInteraction,
        GameCardInteraction,
        ...
    ]
}
```

_GameCardInteraction:_

```
{
    game_link_card: string,  // Id взаимодействия
    id: string,  // Id карты персонажа
    name: string,  // Название карты персонажа
    description: string,  // Описание карты персонажа
    type: string  // Тип карты персонажа
}
```

_PlayerCardInfo:_

```
{
    id: string,  // Id карты персонажа
    name: string,  // Название карты
    description: string,  // Описание карты
    type: string,  // Тип карты
    negative: [  // Список негативных взаимодействий
        PlayerCardInteraction,
        PlayerCardInteraction,
        ...
    ],
    positive: [  // Список позитивных взаимодействий
        PlayerCardInteraction,
        PlayerCardInteraction,
        ...
    ]
}
```

_PlayerCardInteraction:_

```
{
    player_link_card: string,  // Id взаимодействия
    id: string,  // Id игровой карты
    name: string,  // Название игровой карты
    description: string,  // Описание игровой карты
    type: string  // Тип игровой карты
}
```

### `ws/player/`

_Players:_

```
[
    PlayerInfo,
    PlayerInfo,
    ...
]
```

_PlayerInfo:_

```
{
    id: string,  // Id игрока
    name: string,  // Имя игрока
    games: number  // Количество игр
}
```

### `ws/games/`

_Games:_

```
{
    active: [  // Список активных игр
        ActiveGameInfo,
        ActiveGameInfo,
        ...
    ]
    archive: [  // Список архивных игр
        ArchiveGameInfo,
        ArchiveGameInfo,
        ...
    ]
    avaliable: [  // Список доступных игр
        AvaliableGameInfo,
        AvaliableGameInfo,
        ...
    ]
}
```

_ActiveGameInfo:_

```
{
    id: string,  // Id игры
    name: string,  // Название игры
    players: number,  // Количество игроков
    round: number,  // Текущий раунд
    is_voting: boolean,  // Идёт ли голосование
    disaster: string,  // Название катастрофы
    current_player: number  // Текущий игрок
}
```

_ArchiveGameInfo:_

```
{
    id: string,  // Id игры
    name: string,  // Название игры
    players: number,  // Количество игроков
    disaster: string // Название катастрофы
}
```

_AvaliableGameInfo:_

```
{
    id: string,  // Id игры
    name: string,  // Название игры
    players: number,  // Количество игроков
    password: boolean  // Есть ли пароль
}
```

### `ws/game/<uuid:id>/`

_Game:_

```
{
    game: GameInfo  // Информация об игре
    risks: [  // Список угроз или карт бункера
        Risk,
        Risk,
        ...
    ]
    players: [  // Список игроков
        Player,
        Player,
        ...
    ]
    player: Player  // Игрок, сделавший запрос
}
```

_GameInfo:_

```
{
    id: string,  // Id игры
    name: string,  // Название игры
    current_player: number,  // Номер текущего игрока
    is_voting: boolean,  // Идёт ли голосование
    round: number,  // Текущий раунд
    disaster: Disaster | null  // Катастрофа
}
```

_Disaster:_

```
{
    id: string,  // Id катастрофы
    name: string,  // Название катастрофы
    description: string  // Описание катастрофы
}
```

_Risk:_

```
{
    id: string,  // Id угрозы или карты бункера
    is_open: boolean,  // Открыта ли карта
    type: string,  // Тип карты
    name: string,  // Название карты
    description: string  // Описание карты
}
```

_Player:_

```
{
    id: string,  // Id Игрока
    name: string,  // Имя игрока
    vote: string,  // Id игрока, за которого голос
    is_ready: boolean,  // Готов ли игрок
    is_in_game: boolean,  // Жив ли игрок
    number: number,  // Номер игрока
    cards: [  // Список карт игрока
        Card,
        Card,
        ...
    ]
}
```

_Card:_

```
{
    id: string,  // Id карты
    is_open: boolean,  // Открыта ли карта
    name: string,  // Название карты
    description: string,  // Описание карты
    type: string,  // Тип карты
    interactions: [  // Список взаимодействий
        Interaction,
        Interaction,
        ...
    ]
}
```

_Interaction:_

```
{
    type: string,  // Тип взаимодействия
    game_card: string | null,  // Название игровой карты
}
```

## База данных (БД)

### Game:

-   id - Id игры
-   name - Название игры
-   round - Номер раунда
-   current_player - Текущий игрок
-   password - Пароль
-   is_voting - Идёт ли голосование
-   disaster_id - Id катастрофы

### GameRisk:

-   id - Id карты угрозы или карты бункера текущей игры
-   is_open - Открыта ли карта
-   game_id - Id игры
-   risk_id - Id игровой карты

### GameCard:

-   id - Id игровой карты
-   name - Название игровой карты
-   description - Описание игровой карты
-   type - Тип игровой карты (RISK | BUNKER | DISASTER)

### GamePlayerCardsInteraction:

-   id - Id взаимодействия
-   type - Тип взаимодействия (POSITIVE | NEGATIVE)
-   game_card_id - Id игровой карты
-   player_card_id - Id карты персонажа

### PlayerCard:

-   id - Id карты персонажа
-   name - Название карты персонажа
-   description - Описание карты персонажа
-   type - Тип карты персонажа (BIOLOGY | HEALTH | HOBBY | BAGGAGE | FACTS | PROFESSIONS)

### PlayerGame:

-   id - Id игрока
-   number - Номер игрока в игре
-   is_ready - Готов ли игрок
-   is_in_game - Жив ли игрок
-   vote - Id игрока, за которого голос
-   game_id - Id игры
-   player_id - Id пользователя

### Player:

-   id - Id пользователя
-   name - Имя пользователя
-   is_admin - Админ ли пользователь

### PlayerInGameCard:

-   id - Id карты игрока
-   is_open - Открыта ли карта игрока
-   player_id - Id игрока
-   player_card_id - Id карты персонажа
